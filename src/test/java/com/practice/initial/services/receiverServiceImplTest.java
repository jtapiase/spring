package com.practice.initial.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import com.practice.initial.repositories.receiverRepository;
import com.practice.initial.models.receiver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class receiverServiceImplTest {

	@Mock
	receiverRepository receiverRepository;
	
	@InjectMocks
	receiverServiceImpl receiverServiceImpl;

	private receiver receiver;
	private receiver receiver2;
	
	@Before
	public void tearDown() {
		
		receiver = new receiver("storeNameTest", "senderNameTest", "Calle falsa 123", "SpringField", 001100);
		receiver2 = new receiver("storeNameTest2", "senderNameTest2", "Calle falsa 123", "SpringField2", 001100);
	}
	
	@Test
	public void addReceiver() {
		
		//when(receiverServiceImpl.addReceiver(receiver)).thenReturn(receiver);
		when(receiverRepository.insert(receiver)).thenReturn(receiver);
		
		receiver returnedReceiver = receiverServiceImpl.addReceiver(receiver);
		
		assertNotNull(returnedReceiver);
		assertEquals(returnedReceiver.getStoreName(), receiver.getStoreName());
	}
	
	@Test
	public void getAllReceivers() {
		
		when(receiverRepository.findAll()).thenReturn(Arrays.asList(receiver, receiver2));
		
		List<receiver> returnedReceivers = receiverServiceImpl.getAllReceivers();
		
		assertNotNull(returnedReceivers);
		assertEquals(2, returnedReceivers.size());
	}
	
}
