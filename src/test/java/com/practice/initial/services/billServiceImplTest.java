package com.practice.initial.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import com.practice.initial.exceptions.billNotFoundException;
import com.practice.initial.models.bill;
import com.practice.initial.models.product;
import com.practice.initial.models.receiver;
import com.practice.initial.models.sender;
import com.practice.initial.repositories.billRepository;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
//import static org.mockito.Mockito.doNothing;
import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class billServiceImplTest {

	@Mock
	billRepository billRepository;
	
	@Mock
	productServiceImpl productService;
	
	@Mock
	senderServiceImpl senderService;
	
	@Mock
	receiverServiceImpl receiverService;
	
	@InjectMocks
	billServiceImpl billServiceImpl;
	
	private product product;
	private product product2;
	private List<product> products;
	private receiver receiver;
	private sender sender;
	private bill bill;
	
	@Before
	public void tearDown() throws billNotFoundException {
		
		product = new product(2, "Camisas", 250);
		product2 = new product(2, "Bolsos", 120);
		products = Arrays.asList(product,product2);
		receiver = new receiver("storeNameTest", "senderNameTest", "Calle falsa 123", "SpringField", 001100);
		sender = new sender("abc", "def", "calle123", 123);
		
		bill = new bill(products, receiver, sender);
	
	}
	
	@Test
	public void getAllBills() {
		
		when(billServiceImpl.getAllBills()).thenReturn(Arrays.asList(bill));
		List<bill> bills = billRepository.findAll();
		
		assertNotNull(bills);
		assertEquals(1, bills.size());
		
	}

	@Test
	public void addBill() {
		when(billServiceImpl.addBill(bill)).thenReturn(bill);
		bill billReturned = billServiceImpl.addBill(bill);
		
		assertNotNull(billReturned);
		assertEquals(billReturned.getCode(), bill.getCode());
	
	}
	/*	
	@Test
	public void deleteBill() {
		List<bill> bills = billServiceImpl.getAllBills();
				
		doNothing().when(billRepository).delete(bill);
		bills.remove(bill);
		
		assertFalse(bills.contains(bill));
		
	}
	*/
	@Test
	public void updateBillTest() {
	
		when(billServiceImpl.updateBill(bill)).thenReturn(bill);
		bill billUpdate = billServiceImpl.updateBill(bill);
		
		assertEquals(billUpdate, billServiceImpl.updateBill(bill));
		
	}
	
	@Test
	public void findByCodeTest() throws billNotFoundException {
		/*
		when(billServiceImpl.findBillByCode(bill.getCode())).thenReturn(bill);
		bill billReturned = billServiceImpl.findBillByCode(bill.getCode());
	
		assertNotNull(billReturned);
		assertEquals(billReturned.getCode(), bill.getCode());
		*/
	}

}
