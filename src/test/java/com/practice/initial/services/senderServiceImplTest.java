package com.practice.initial.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import com.practice.initial.repositories.senderRepository;
import com.practice.initial.models.sender;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class senderServiceImplTest {

	@Mock
	senderRepository senderRepository;
	
	@InjectMocks
	senderServiceImpl senderServiceImpl;
	
	private sender sender;
	private sender sender2;
	
	@Before
	public void tearDown() {
		
		//sender = new sender(new Integer(12345), "Camilo");
		//sender2 = new sender(new Integer(23456), "Andres");
		sender = new sender("abc", "def","calle123", 123);
	}
	
	@Test
	public void addSender() {
		
		when(senderRepository.insert(sender)).thenReturn(sender);
	
		sender returnedSender = senderServiceImpl.addSender(sender);
		
		assertNotNull(returnedSender);
		assertEquals(returnedSender.getStoreName(), sender.getStoreName());
	}
	
	@Test
	public void getAllSenders() {
		
		when(senderRepository.findAll()).thenReturn(Arrays.asList(sender,sender2));
	
		List<sender> returnedSenders = senderServiceImpl.getAllSenders();
		
		assertNotNull(returnedSenders);
		assertEquals(2, returnedSenders.size());
	}
	
	
}
