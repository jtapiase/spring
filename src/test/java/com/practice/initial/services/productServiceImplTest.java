package com.practice.initial.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import com.practice.initial.repositories.productRepository;
import com.practice.initial.models.product;
import com.practice.initial.models.sender;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class productServiceImplTest {

	@Mock 
	productRepository productRepository;
	
	@InjectMocks
	productServiceImpl productServiceImpl;
	
	private product product;
	private product product2;
	private sender sender;
	
	@Before
	public void tearDown() {
		
		product = new product(2, "camisetas deportivas", 12000);
		product2 = new product(2, "boxers amarillos pa la suerte", 12012);
		
		sender = new sender("abc", "def", "calle123", 123);
		
	}
	
	@Test
	public void addProduct() {
		
		when(productRepository.insert(product)).thenReturn(product);
		
		product receivedProduct = productServiceImpl.addProduct(product);
		
		assertNotNull(receivedProduct);
		assertEquals(receivedProduct.getCode(), product.getCode());
	
	}
	/*
	@Test
	public void getAllProductByMySender() {
		
		when(productRepository.findAllByMySender(sender.getStoreName())).thenReturn(Arrays.asList(product, product2));
		
		List<product> returnedProducts = productServiceImpl.getAllProductsByMySender(sender.getStoreName());
		
		assertNotNull(returnedProducts);
		//assertEquals("Horacio", returnedProducts.get(0).getMySender());
		assertEquals(2, returnedProducts.size());
	}
	*/
}
