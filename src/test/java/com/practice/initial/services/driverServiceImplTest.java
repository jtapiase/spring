package com.practice.initial.services;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import com.practice.initial.repositories.driverRepository;
import com.practice.initial.services.driverServiceImpl;
import com.practice.initial.models.driver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class driverServiceImplTest {

	@Mock
	driverRepository driverRepository;
	
	@InjectMocks
	driverServiceImpl driverServiceImpl;
	
	private driver driver;
	private driver driver2;
	
	@Before 
	public void tearDown() {
		
		driver = new driver(new Integer(12345), "Horacio");
		driver2 = new driver(new Integer(23456), "Jacinto");
		
	}
	
	@Test
	public void addDriver() {
		
		when(driverServiceImpl.addDriver(driver)).thenReturn(driver);
		
		driver driverReturned = driverServiceImpl.addDriver(driver);
		assertNotNull(driverReturned);
		assertEquals(driverReturned.getName(), driver.getName());
	}
	
	@Test
	public void updateDriver() {
		
		when(driverServiceImpl.updateDriver(driver)).thenReturn(driver);
		
		driver updatedDriver = driverServiceImpl.updateDriver(driver);
		
		assertNotNull(updatedDriver);
		assertEquals(updatedDriver.getName(), driver.getName());
	
	}

	@Test
	public void getAllDrivers() {
		
		when(driverServiceImpl.getAllDrivers()).thenReturn(Arrays.asList(driver, driver2));
		
		List<driver> drivers = driverServiceImpl.getAllDrivers();
		
		assertNotNull(drivers);
		assertEquals(2, drivers.size());
	}
}
