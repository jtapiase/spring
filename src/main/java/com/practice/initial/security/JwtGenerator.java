package com.practice.initial.security;

import com.practice.initial.models.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class JwtGenerator {


    public String generate(JwtUser jwtUser) {

    	LocalDate date = LocalDate.now().plusDays(1);
        Claims claims = Jwts.claims()
                .setSubject(jwtUser.getUserName());
        claims.put("role", jwtUser.getRole());


        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, "youtube")
                .setExpiration(new Date(date.getYear(), date.getMonthValue(), date.getDayOfMonth()))
                .compact();
    }
}
