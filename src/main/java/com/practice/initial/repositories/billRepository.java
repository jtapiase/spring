package com.practice.initial.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.practice.initial.models.bill;
import com.practice.initial.models.sender;


@Repository("billRepository")
public interface billRepository  extends MongoRepository<bill, Integer>{
    
	void deleteByCode(Integer code);
    List<bill> findBillBySender(sender sender);

}