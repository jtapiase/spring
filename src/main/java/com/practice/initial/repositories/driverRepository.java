package com.practice.initial.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.practice.initial.models.driver;

@Repository("driverRepository")
public interface driverRepository extends MongoRepository<driver, Integer>{
	
	void deleteByName(String name);
	List<driver> findByName(String name);
	
}
