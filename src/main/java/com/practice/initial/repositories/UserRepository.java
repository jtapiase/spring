package com.practice.initial.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.practice.initial.models.JwtUser;

public interface UserRepository extends MongoRepository<JwtUser, Integer> {
	JwtUser findByUserName(String name);
}
