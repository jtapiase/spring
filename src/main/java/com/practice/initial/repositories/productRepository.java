package com.practice.initial.repositories;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.practice.initial.models.product;

@Repository("productRepository")
public interface productRepository extends MongoRepository<product, Integer> {
	
	//List<product> findAllByMySender(String sender);

}
