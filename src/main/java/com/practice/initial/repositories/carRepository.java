package com.practice.initial.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.practice.initial.models.car;

@Repository("carRepository")
public interface carRepository extends MongoRepository<car, String>{

}
