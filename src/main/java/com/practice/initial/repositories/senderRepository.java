package com.practice.initial.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.practice.initial.models.sender;

@Repository("senderRepository")
public interface senderRepository extends MongoRepository<sender, String> {
	
	void deleteByStoreName(String name);
	sender findByStoreNameIgnoreCase(String name);
	
}
