package com.practice.initial.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.practice.initial.models.pickUpOrder;

@Repository("orderRepository")
public interface orderRepository extends MongoRepository<pickUpOrder, Integer> {

	List<pickUpOrder> findAllBySender(String storeName);
}
