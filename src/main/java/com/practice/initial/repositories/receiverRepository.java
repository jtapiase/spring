package com.practice.initial.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.practice.initial.models.receiver;

@Repository("receiverRepository")
public interface receiverRepository extends MongoRepository<receiver, Integer>{
	
	receiver findByStoreName(String storeName);
}
