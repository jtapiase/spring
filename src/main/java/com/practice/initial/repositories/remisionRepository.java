package com.practice.initial.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.practice.initial.models.remision;

@Repository("remisionRepository")
public interface remisionRepository extends MongoRepository<remision, Integer>{

}
