package com.practice.initial.services;

import java.util.List;

import com.practice.initial.models.pickUpOrder;

public interface orderService {

	pickUpOrder addOrder(pickUpOrder order);
	List<pickUpOrder> getOrders();
	pickUpOrder updateOrder(pickUpOrder order);
	void deleteOrder(Integer id);
	List<pickUpOrder> getOrderBySender(String storeName);
}
