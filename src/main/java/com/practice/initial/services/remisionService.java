package com.practice.initial.services;

import java.util.List;

import com.practice.initial.exceptions.billNotFoundException;
import com.practice.initial.models.remision;

public interface remisionService {
	List<remision> getAllRemision();
	remision addRemision(remision remision) throws billNotFoundException;
	void deleteRemision(Integer code);
	remision updateRemision(remision remision);
}
