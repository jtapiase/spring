package com.practice.initial.services;

import com.practice.initial.exceptions.billNotFoundException;
import com.practice.initial.exceptions.senderNotFoundException;
import com.practice.initial.models.bill;
import com.practice.initial.models.sender;

import java.util.List;


public interface billService {
    
	List<bill> getAllBills();
    bill addBill(bill bill);
    bill updateBill(bill bill) throws billNotFoundException;
    List<bill> findBillBySender(sender sender) throws senderNotFoundException;

}