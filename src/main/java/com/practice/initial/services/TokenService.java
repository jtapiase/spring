package com.practice.initial.services;

import com.practice.initial.models.tokenObject;

public interface TokenService {

	tokenObject checkUserCredentials(String name, String pass);
	
}
