package com.practice.initial.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.practice.initial.exceptions.billNotFoundException;
import com.practice.initial.models.bill;
import com.practice.initial.models.remision;
import com.practice.initial.repositories.remisionRepository;
import com.practice.initial.repositories.billRepository;

@Service
public class remisionServiceImpl implements remisionService {

	@Autowired
	@Qualifier("remisionRepository")
	private remisionRepository remisionRepository;

	@Autowired
	@Qualifier("billRepository")
	private billRepository billRepository;

	@Override
	public List<remision> getAllRemision() {
		return remisionRepository.findAll();
	}

	@Override
	public remision addRemision(remision remision) throws billNotFoundException {
		Integer code;
		List<Integer> codes = this.codes();

		if (codes.size() <= 0)
			code = 0;
		else
			code = codes.get(codes.size() - 1);
		remision.setCode(code);
		remision.setDate();

		List<bill> bills = billRepository.findAll();
		Integer billRemisionCode = remision.getBillCode();

		List<Integer> bcodes = new ArrayList<>();
		for (bill b : bills) {
			bcodes.add(b.getCode());
		}

		if (bcodes.contains(billRemisionCode)){
			return remisionRepository.insert(remision);
		} else {
			throw new billNotFoundException("la factura no existe");
		}
	}

	@Override
	public void deleteRemision(Integer code) {
		remisionRepository.deleteById(code);
	}

	@Override
	public remision updateRemision(remision remision) {
		return remisionRepository.save(remision);
	}

	private List<Integer> codes() {
		List<Integer> codes = new ArrayList<>();
		List<remision> remisions = this.remisionRepository.findAll();
		for (remision remision : remisions) {
			codes.add(remision.getCode());
		}

		Collections.sort(codes);
		return codes;
	}
}
