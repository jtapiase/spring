package com.practice.initial.services;

import java.util.List;
import com.practice.initial.models.receiver;

public interface receiverService {
	receiver addReceiver(receiver receiver);
	receiver updateReceiver(receiver receiver);
	void deleteReceiverById(Integer id);
	List<receiver> getAllReceivers();
	
}
