package com.practice.initial.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.practice.initial.models.car;
import com.practice.initial.repositories.carRepository;

@Service("carService")
public class carServiceImpl implements carService {

	@Autowired
	@Qualifier("carRepository")
	carRepository carRepository;
	
	public car addCar(car car) {
		
		return this.carRepository.insert(car);
		
	}


	public void deleteCar(String carPlate) {

		this.carRepository.deleteById(carPlate);
		
	}
	
	public List<car> getCars(){
		return this.carRepository.findAll();
	}

}
