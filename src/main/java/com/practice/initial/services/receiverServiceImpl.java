package com.practice.initial.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.practice.initial.repositories.receiverRepository;
import org.springframework.stereotype.Service;
import com.practice.initial.models.receiver;

import static java.util.Objects.requireNonNull;

@Service
public class receiverServiceImpl implements receiverService{

	@Autowired
	@Qualifier("receiverRepository")
	private receiverRepository receiverRepository;

	@Override
	public receiver addReceiver(receiver receiver) {
		requireNonNull(receiver.getStoreName(), "Debe ingresarse nombre del establecimiento");
		requireNonNull(receiver.getReceiverName(), "Debe ingresarse nombre del destinatario");
		requireNonNull(receiver.getAddress(), "Debe ingresar dirección de destino");
		requireNonNull(receiver.getCity(), "Debe ingresar la ciudad de destino");
		requireNonNull(receiver.getTel(), "Debe ingresarse un numero telefónico");
		
		receiver receiverResult = receiverRepository.findByStoreName(receiver.getStoreName());
		if(receiverResult == null)
			return receiverRepository.insert(receiver);
		return receiver;
	}

	@Override
	public List<receiver> getAllReceivers() {
		return receiverRepository.findAll();
	}

	@Override
	public receiver updateReceiver(receiver receiver) {
		requireNonNull(receiver.getStoreName(), "Debe ingresarse nombre del establecimiento");
		requireNonNull(receiver.getReceiverName(), "Debe ingresarse nombre del destinatario");
		requireNonNull(receiver.getAddress(), "Debe ingresar dirección de destino");
		requireNonNull(receiver.getCity(), "Debe ingresar la ciudad de destino");
		
		return receiverRepository.save(receiver);
	}

	@Override
	public void deleteReceiverById(Integer id) {
		receiverRepository.deleteById(id);
	}


}
