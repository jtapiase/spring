package com.practice.initial.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.practice.initial.repositories.driverRepository;
import org.springframework.stereotype.Service;

import com.practice.initial.models.car;
import com.practice.initial.models.driver;
import com.practice.initial.repositories.carRepository;

import static java.util.Objects.requireNonNull;

@Service
public class driverServiceImpl implements driverService {
	
	@Autowired
	@Qualifier("driverRepository")
	private driverRepository driverRepository;
	
	@Autowired
	@Qualifier("carRepository")
	private carRepository carRepository;
	
	@Override
	public driver addDriver(driver driver) {
		requireNonNull(driver.getId(), "Debe ingresar el id del conductor");
		requireNonNull(driver.getName(), "Debe ingresar el nombre del conductor");
		
		return driverRepository.insert(driver);
	}

	@Override
	public void deleteDriver(String name) {
		driverRepository.deleteByName(name);		
	}

	@Override
	public driver updateDriver(driver driver) {
		requireNonNull(driver.getId(), "Debe ingresar el id del conductor");
		requireNonNull(driver.getName(), "Debe ingresar el nombre del conductor");
		
		return driverRepository.save(driver);
	}

	@Override
	public List<driver> getAllDrivers(){
		return driverRepository.findAll();
	}

}
