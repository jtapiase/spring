package com.practice.initial.services;

import java.util.List;

import com.practice.initial.models.car;

public interface carService {
	
	car addCar(car car);
	void deleteCar(String carPlate);
	List<car> getCars();

}
