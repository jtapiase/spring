package com.practice.initial.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.practice.initial.models.JwtUser;
import com.practice.initial.models.tokenObject;
import com.practice.initial.repositories.UserRepository;
import com.practice.initial.security.JwtGenerator;

@Service("tokenService")
public class TokenServiceImpl implements TokenService {

	@Autowired
	@Qualifier("userRepository")
	private UserRepository userRepository;
	
	private JwtGenerator jwtGenerator;

    public TokenServiceImpl(JwtGenerator jwtGenerator) {
        this.jwtGenerator = jwtGenerator;
    }

	@Override
	public tokenObject checkUserCredentials(String name, String pass) {
		JwtUser user = userRepository.findByUserName(name);
		
		if(user.getUserName().equals(name) && user.getPassword().equals(pass)) {
			tokenObject tokenObject = new tokenObject();
			String token = jwtGenerator.generate(user);
			tokenObject.addToken(token);
			return tokenObject;
		}
		throw new RuntimeException("Bad credentials");
	/*
		System.out.println(user.toString());
		return "tres";
	*/}

	
}
