package com.practice.initial.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.practice.initial.repositories.senderRepository;
import org.springframework.stereotype.Service;
import com.practice.initial.models.sender;
import java.util.List;

import static java.util.Objects.requireNonNull;

@Service
public class senderServiceImpl implements senderService{

	@Autowired
	@Qualifier("senderRepository")
	private senderRepository senderRepository;

	@Override
	public sender addSender(sender sender) {
		requireNonNull(sender.getStoreName(), "Debe ingresarse nombre del establecimiento");
		requireNonNull(sender.getNameSender(), "Debe ingresarse nombre del remitente");
		requireNonNull(sender.getAddress(), "Debe ingresarse la direccion del remitente");
		requireNonNull(sender.getTel(), "Debe ingresarse numero telefónico");
		sender senderResult = senderRepository.findByStoreNameIgnoreCase(sender.getStoreName());
		if(senderResult == null) 
			return senderRepository.insert(sender);
		
		return sender;
	}

	@Override
	public List<sender> getAllSenders() {
		return senderRepository.findAll();
	}

	@Override
	public void deleteByStoreName(String id) {
		senderRepository.deleteByStoreName(id);
	}

	@Override
	public sender updateSender(sender sender) {
		//requireNonNull(sender.getName(), "Se debe ingresar el nombre del remitente");
		//requireNonNull(sender.getId(), "Se debe ingresar el Id/NIT del remitente");

		return senderRepository.save(sender);
	}
	
	@Override
	public sender getSenderByName(String senderName) {
		return senderRepository.findByStoreNameIgnoreCase(senderName);
	}

}
