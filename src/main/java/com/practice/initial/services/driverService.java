package com.practice.initial.services;

import java.util.List;
import com.practice.initial.exceptions.driverNotFoundException;
import com.practice.initial.models.car;
import com.practice.initial.models.driver;

public interface driverService {
	driver addDriver(driver driver);
	void deleteDriver(String name) throws driverNotFoundException;
	driver updateDriver(driver driver);
	List<driver> getAllDrivers();
}
