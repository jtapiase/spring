package com.practice.initial.services;

import com.practice.initial.exceptions.billNotFoundException;
import com.practice.initial.exceptions.senderNotFoundException;
import com.practice.initial.models.bill;
import com.practice.initial.models.product;
import com.practice.initial.models.sender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.practice.initial.repositories.billRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class billServiceImpl implements billService{
    
	@Autowired
	@Qualifier("billRepository")
	private billRepository billRepository;
	
	@Autowired
	@Qualifier("productServiceImpl")
	private productServiceImpl productService;
	
	@Autowired
	@Qualifier("senderServiceImpl")
	private senderServiceImpl senderService;
	
	@Autowired
	@Qualifier("receiverServiceImpl")
	private receiverServiceImpl receiverService;
	
    public List<bill> getAllBills(){
        return billRepository.findAll();
    }

    public bill addBill(bill bill){
    	senderService.addSender(bill.getSender());
    	receiverService.addReceiver(bill.getReceiver());
    	bill.getProducts().forEach(producto -> {
    		productService.addProduct(producto);
    	});
    	
    	Integer code;
    	List<Integer> codes = this.codes();
    	
    	if(codes.size()<=0)
    		code=0;
    	else
    		code = codes.get(codes.size()-1);
    	bill.setCode(code);
    	
    	bill.setDate();
    	return billRepository.insert(bill);
    }

    public void deleteBill(Integer code) throws billNotFoundException{   
        this.billRepository.deleteByCode(code);
    }

    public bill updateBill(bill bill){
        return this.billRepository.save(bill);
    }
    
    public List<bill> findBillBySender(sender sender) throws senderNotFoundException {
    	return this.billRepository.findBillBySender(sender);
    }
    
    private List<Integer> codes(){
		List<Integer> codes = new ArrayList<>();
		List<bill> bills = this.billRepository.findAll();
		for(bill bill:bills) {
			codes.add(bill.getCode());
		}
		
		Collections.sort(codes);
		return codes;
	}
	
}