package com.practice.initial.services;

import java.util.List;

import com.practice.initial.models.product;

public interface productService {
	product addProduct(product product);
	product updateProduct(product product);
	void deleteProductById(Integer id);
	List<product> getProducts();
}
