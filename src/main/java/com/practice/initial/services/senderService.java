package com.practice.initial.services;

import java.util.List;
import com.practice.initial.models.sender;

public interface senderService {
	sender addSender(sender sender);
	List<sender> getAllSenders();
	sender updateSender(sender sender);
	void deleteByStoreName(String name);
	sender getSenderByName(String senderName);
}
