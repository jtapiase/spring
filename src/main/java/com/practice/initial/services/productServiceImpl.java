package com.practice.initial.services;

import java.util.List;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.practice.initial.repositories.productRepository;
import org.springframework.stereotype.Service;
import com.practice.initial.models.product;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;

@Service
public class productServiceImpl implements productService{

	@Autowired
	@Qualifier("productRepository")
	private productRepository productRepository;
	
	@Override
	public product addProduct(product product) {
		requireNonNull(product.getCant(), "Debe ingresarse una cantidad valida");
		requireNonNull(product.getDescription(), "Debe ingresarse una descripción");
		/*requireNonNull(product.getMySender(), "Debe especificarse un remitente");
		requireNonNull(product.getMyReceiver(), "Debe especificarse un destinatario");
		*/
		
		List<Integer> codes = this.codes();
		Integer code;
		if(codes.size()<=0)
    		code=0;
    	else
    		code = codes.get(codes.size()-1);
    	product.setCode(code);
    	
		return productRepository.insert(product);
	}
	
	@Override
	public List<product> getProducts() {
		return productRepository.findAll();
	}
	 
	@Override
	public product updateProduct(product product) {
		requireNonNull(product.getCant(), "Debe ingresarse una cantidad valida");
		requireNonNull(product.getDescription(), "Debe ingresarse una descripción");
		/*requireNonNull(product.getMySender(), "Debe especificarse un remitente");
		requireNonNull(product.getMyReceiver(), "Debe especificarse un destinatario");
		*/
		return productRepository.save(product);
	}

	@Override
	public void deleteProductById(Integer id) {
		productRepository.deleteById(id);
	}
	
	private List<Integer> codes(){
		List<Integer> codes = new ArrayList<>();
		List<product> productos = this.productRepository.findAll();
		for(product product:productos) {
			codes.add(product.getCode());
		}
		
		Collections.sort(codes);
		return codes;
	}

}
