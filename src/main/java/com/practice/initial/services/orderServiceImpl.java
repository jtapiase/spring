package com.practice.initial.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practice.initial.models.bill;
import com.practice.initial.models.pickUpOrder;
import com.practice.initial.repositories.orderRepository;

@Service("orderService")
public class orderServiceImpl implements orderService{

	@Autowired
	private orderRepository orderRepository;
	
	@Autowired
	private senderService senderService;
	
	@Autowired
	private receiverService receiverService;
	
	@Autowired
	private productService productService;
	
	
	@Override
	public pickUpOrder addOrder(pickUpOrder order) {
		senderService.addSender(order.getSender());
    	receiverService.addReceiver(order.getReceiver());
    	order.getProducts().forEach(producto -> {
    		productService.addProduct(producto);
    	});
    	
    	Integer code;
    	List<Integer> codes = this.codes();
    	
    	if(codes.size()<=0)
    		code=0;
    	else
    		code = codes.get(codes.size()-1);
    	order.setCode(code);
    	
    	order.setDate();
    	return orderRepository.insert(order);
	}

	@Override
	public List<pickUpOrder> getOrders() {
		// TODO Auto-generated method stub
		return orderRepository.findAll();
	}

	@Override
	public pickUpOrder updateOrder(pickUpOrder order) {
		// TODO Auto-generated method stub
		return orderRepository.save(order);
	}

	@Override
	public void deleteOrder(Integer id) {
		orderRepository.deleteById(id);
		
	}

	@Override
	public List<pickUpOrder> getOrderBySender(String storeName) {
		return orderRepository.findAllBySender(storeName);
	}
	
	  private List<Integer> codes(){
			List<Integer> codes = new ArrayList<>();
			List<pickUpOrder> orders = this.orderRepository.findAll();
			for(pickUpOrder order: orders) {
				codes.add(order.getCode());
			}
			
			Collections.sort(codes);
			return codes;
		}

}
