package com.practice.initial.tools;

import java.io.IOException;
import java.time.LocalDateTime;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

    public LocalDateTime deserialize(JsonParser parser, DeserializationContext context) throws IOException {

        if (parser.getCurrentToken().equals(JsonToken.VALUE_STRING)) {
            String rawDate = parser.getText();
            return LocalDateTime.parse(rawDate);
        } else {
            throw context.wrongTokenException(parser, JsonToken.VALUE_STRING, "Expected string.");
    }
}
}