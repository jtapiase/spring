package com.practice.initial.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import com.practice.initial.services.receiverServiceImpl;
import com.practice.initial.models.receiver;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/destinatario")
@RestController
public class receiverController {

	@Autowired
	@Qualifier("receiverServiceImpl")
	private receiverServiceImpl receiverService;
	
	@GetMapping
	public List<receiver> getAllReceivers(){
		return receiverService.getAllReceivers();
	}
	
	@PostMapping
	public receiver addReceiver(@RequestBody receiver receiver){
		return receiverService.addReceiver(receiver);
	}
	
	@PutMapping
	public receiver updateReceiver(@RequestBody receiver receiver) {
		return receiverService.updateReceiver(receiver);
	}
	
	@DeleteMapping("/{Id}")
	public void deleteReceiver(@PathVariable("Id") Integer id) {
		receiverService.deleteReceiverById(id);
	}

}
