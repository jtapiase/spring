package com.practice.initial.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.practice.initial.exceptions.billNotFoundException;
import com.practice.initial.models.remision;
import com.practice.initial.services.remisionServiceImpl;

import static java.util.Objects.requireNonNull;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/remisiones")
@RestController
public class remisionController {

	@Autowired
	@Qualifier("remisionServiceImpl")
	private remisionServiceImpl remisionService;
	
	@GetMapping
	public List<remision> getRemisions(){
		
		return this.remisionService.getAllRemision();
	
	}
	
	@PostMapping
	public remision addRemision(@RequestBody @Valid remision remision ) throws billNotFoundException{
		requireNonNull(remision.getBillCode(), "Debe ingresar una factura");
		requireNonNull(remision.getDriver(), "Debe ingresar un conductor");
		requireNonNull(remision.getCar(), "Debe ingresar un vehiculo");

		return this.remisionService.addRemision(remision);
	}
}
