package com.practice.initial.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.practice.initial.models.car;
import com.practice.initial.services.carServiceImpl;
import static java.util.Objects.requireNonNull;

import java.util.List;

@RestController
@RequestMapping("/api/car")
@CrossOrigin(origins = "http://localhost:4200")
public class carController {
	
	@Qualifier("carService")
	@Autowired
	carServiceImpl carService;
	
	@GetMapping
	public List<car> getCars(){
		return this.carService.getCars();
	}
	
	@PostMapping
	public car addCar(@RequestBody @Valid car car) {
		requireNonNull(car.getCarPlate(), "Debe ingresarse la placa del vehiculo");
		requireNonNull(car.getName(), "Debe ingresarse el nombre/marca del vehiculo");
		
		return carService.addCar(car);
	}

	@DeleteMapping("/{carPlate}")
	public void deleteCar(@PathVariable("carPlate") String carPlate) {
		requireNonNull(carPlate, "Debe ingresarse la placa del vehiculo a eliminar");
		
		carService.deleteCar(carPlate);
	}
}
