package com.practice.initial.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import com.practice.initial.services.productServiceImpl;
import com.practice.initial.models.product;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/productos")
@RestController
public class productController {

	@Autowired
	@Qualifier("productServiceImpl")
	private productServiceImpl productService;
	
	@GetMapping
	public List<product> getAllProducts(){
		return productService.getProducts();
	}
	
	@PostMapping
	public product addProduct(@RequestBody product producto) {
		return productService.addProduct(producto);
	}
	
	@PutMapping
	public product updateProduct(@RequestBody product product) {
		return productService.updateProduct(product);
	}
	
	@DeleteMapping("/{code}")
	public void deleteProduct(@PathVariable("code") Integer code) {
		productService.deleteProductById(code);
	}
}
