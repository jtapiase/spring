package com.practice.initial.controllers;

import com.practice.initial.exceptions.driverNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import com.practice.initial.services.driverServiceImpl;
import com.practice.initial.models.driver;
import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/conductor")
@RestController
public class driverController {

	@Autowired
	@Qualifier("driverServiceImpl")
	private driverServiceImpl driverService;
	
	@GetMapping
	public List<driver> getDrivers(){
		return driverService.getAllDrivers();
	}
	
	@PostMapping
	public driver addDriver(@RequestBody @Valid driver driver) {
		return driverService.addDriver(driver);
	}
	
	@DeleteMapping("/{name}")
	public void deleteDriver(@PathVariable("name") String name) throws driverNotFoundException{
		driverService.deleteDriver(name);
	}
	
	@PutMapping
	public driver updateDriver(@RequestBody @Valid driver driver) {
		return driverService.updateDriver(driver);
	}	
}
