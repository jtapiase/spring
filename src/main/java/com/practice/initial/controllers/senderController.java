package com.practice.initial.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import com.practice.initial.services.senderServiceImpl;
import com.practice.initial.models.sender;
import com.practice.initial.exceptions.senderNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/api/remitente")
@CrossOrigin(origins = "http://localhost:4200")
public class senderController {
	
	@Autowired
	@Qualifier("senderServiceImpl")
	private senderServiceImpl senderService;
	
	@GetMapping
	public List<sender> getAllSenders(){
		return senderService.getAllSenders();
	}
	
	@PostMapping
	public sender addSender(@RequestBody sender sender) {
		
		return senderService.addSender(sender);
	}
	
	@DeleteMapping("/{name}")
	public void deteleSender(@PathVariable("name") String name) {
		senderService.deleteByStoreName(name);
	}
	
	@PutMapping
	public sender updateSender(@RequestBody sender sender) {
		return senderService.updateSender(sender);
	}
	
	@GetMapping("/{StoreName}")
	public sender getSenderByStoreName(@PathVariable("StoreName") String storeName) throws senderNotFoundException {
		
		sender sender = senderService.getSenderByName(storeName);
		if(sender != null)
			return sender;
		else {
			throw new senderNotFoundException("No se encontró el cliente con nombre: " + storeName);
		}
	}
}
