package com.practice.initial.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import com.practice.initial.services.billServiceImpl;
import com.practice.initial.services.senderServiceImpl;
import com.practice.initial.exceptions.billNotFoundException;
import com.practice.initial.models.bill;

import javax.validation.Valid;
import java.util.List;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;

@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = {"X-Requested-With, Content-Type, Authorisation"})
//@CrossOrigin(allowedHeaders = "Authorisation")

@RestController
@RequestMapping("/api/bill")
public class billController{
    
	@Autowired
	@Qualifier("billServiceImpl")
    private billServiceImpl billService;
	
	@Autowired
	@Qualifier("senderServiceImpl")
    private senderServiceImpl senderService;
	
    @PostMapping
    public bill registerBill(@RequestBody @Valid bill bill){
    	//Checking all the required attributes for each obtained object
    	requireNonNull(bill.getSender().getStoreName(), "Debe ingresarse nombre del establecimiento");
		requireNonNull(bill.getSender().getNameSender(), "Debe ingresarse nombre del remitente");
		requireNonNull(bill.getSender().getAddress(), "Debe ingresarse la direccion del remitente");
		requireNonNull(bill.getSender().getTel(), "Debe ingresarse numero telefónico");
		requireNonNull(bill.getReceiver().getStoreName(), "Debe ingresarse nombre del establecimiento");
		requireNonNull(bill.getReceiver().getReceiverName(), "Debe ingresarse nombre del destinatario");
		requireNonNull(bill.getReceiver().getAddress(), "Debe ingresar dirección de destino");
		requireNonNull(bill.getReceiver().getCity(), "Debe ingresar la ciudad de destino");
        requireNonNull(bill.getProducts(), "Debe ingresarse al menos un producto");
        
        //Returning the saved bill
        return this.billService.addBill(bill);
    }

    @GetMapping
    public List<bill> getBills(){
        return this.billService.getAllBills();
    }

    @PutMapping
    public bill updateBill(@RequestBody @Valid bill bill){
        return this.billService.updateBill(bill);
    }
    
    @GetMapping("/{storeName}")
    public List<bill> getBillByStoreName(@PathVariable("storeName") String storeName) throws billNotFoundException{
    	requireNonNull(storeName, "Debe ingresar nombre del establecimiento a buscar");
    	
    	List<bill> allBills = new ArrayList<>();
    	List<bill> billsResult = new ArrayList<>();
    	allBills = this.billService.getAllBills();

    	for(bill bill: allBills)
    		if((bill.getSender().getStoreName()).equalsIgnoreCase(storeName))
    			billsResult.add(bill);
    	if(billsResult.size() > 0) {
    		return billsResult;
    	}else {
    		throw new billNotFoundException("no se econtró facturas con el nombre: " + storeName);
    	}
    }
    
    
}