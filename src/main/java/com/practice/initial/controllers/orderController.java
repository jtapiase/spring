package com.practice.initial.controllers;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practice.initial.exceptions.billNotFoundException;
import com.practice.initial.models.bill;
import com.practice.initial.models.pickUpOrder;
import com.practice.initial.services.orderService;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/order")
@RestController
public class orderController {

	@Autowired
	@Qualifier("orderService")
	private orderService orderService;
	
	@PostMapping
	public pickUpOrder addOrder(@RequestBody pickUpOrder order) {
		//Checking all the required attributes for each object
    	requireNonNull(order.getSender().getStoreName(), "Debe ingresarse nombre del establecimiento");
		requireNonNull(order.getSender().getNameSender(), "Debe ingresarse nombre del remitente");
		requireNonNull(order.getSender().getAddress(), "Debe ingresarse la direccion del remitente");
		requireNonNull(order.getSender().getTel(), "Debe ingresarse numero telefónico");
		requireNonNull(order.getReceiver().getStoreName(), "Debe ingresarse nombre del establecimiento");
		requireNonNull(order.getReceiver().getReceiverName(), "Debe ingresarse nombre del destinatario");
		requireNonNull(order.getReceiver().getAddress(), "Debe ingresar dirección de destino");
		requireNonNull(order.getReceiver().getCity(), "Debe ingresar la ciudad de destino");
        requireNonNull(order.getProducts(), "Debe ingresarse al menos un producto");
        
       
		return orderService.addOrder(order);
	}
	
	
	@GetMapping("/{storeName}")
	public List<pickUpOrder> getOrder(@PathVariable("storeName") String storeName ) throws billNotFoundException{
	requireNonNull(storeName, "Debe ingresar nombre del establecimiento a buscar");
    	
    	List<pickUpOrder> allOrders = new ArrayList<>();
    	List<pickUpOrder> orderResult = new ArrayList<>();
    	allOrders = this.orderService.getOrders();

    	for(pickUpOrder order: allOrders)
    		if((order.getSender().getStoreName()).equalsIgnoreCase(storeName))
    			orderResult.add(order);
    	if(orderResult.size() > 0) {
    		return orderResult;
    	}else {
    		throw new billNotFoundException("no se econtró ordenes: " + storeName);
    	}
	}
	
	@GetMapping
	public List<pickUpOrder> getOrders(){
		return this.orderService.getOrders();
	}
}
