package com.practice.initial.controllers;

import com.practice.initial.models.JwtUser;
import com.practice.initial.models.tokenObject;
import com.practice.initial.services.TokenService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/token")
@CrossOrigin(origins = "http://localhost:4200")
public class TokenController {

	@Autowired
	@Qualifier("tokenService")
	private TokenService tokenService;

    @PostMapping
    public tokenObject generate(@RequestBody JwtUser jwtUser) {
        
    	return tokenService.checkUserCredentials(jwtUser.getUserName(), jwtUser.getPassword());
    
    }
}

