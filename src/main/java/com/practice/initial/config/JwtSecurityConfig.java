package com.practice.initial.config;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import com.practice.initial.security.JwtAuthenticationEntryPoint;
import com.practice.initial.security.JwtAuthenticationProvider;
import com.practice.initial.security.JwtAuthenticationTokenFilter;
import com.practice.initial.security.JwtSuccessHandler;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@Configuration
public class JwtSecurityConfig extends WebSecurityConfigurerAdapter{

	 @Autowired
	 private JwtAuthenticationProvider authenticationProvider;
	 
	 @Autowired
	 private JwtAuthenticationEntryPoint entryPoint;
	    
	 @Bean
	 public AuthenticationManager authenticationManager() {
		 return new ProviderManager(Collections.singletonList(authenticationProvider));
	 }

	 @Bean
	 public JwtAuthenticationTokenFilter authenticationTokenFilter() {
		 JwtAuthenticationTokenFilter filter = new JwtAuthenticationTokenFilter();
		 filter.setAuthenticationManager(authenticationManager());
		 filter.setAuthenticationSuccessHandler(new JwtSuccessHandler());
		 return filter;
	 }


	 @Override
	 protected void configure(HttpSecurity http) throws Exception {

		 http.csrf().disable()
		 		.authorizeRequests().antMatchers("**/api/**").authenticated()
		 		.and()
		 		.exceptionHandling().authenticationEntryPoint(entryPoint)
		 		.and()
		 		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		 http.addFilterBefore(new CorsFilter(), ChannelProcessingFilter.class);
		 http.addFilterBefore(authenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
		 http.headers().cacheControl();

	 }
}
