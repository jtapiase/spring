package com.practice.initial.models;

import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "senders")
public class sender {
	
	@NotNull(message = "Debe ingresarse nombre del establecimiento")
	private String storeName;
	
	@NotNull(message = "Debe ingresarse nombre del remitente")
	private String nameSender;
	
	@NotNull(message = "Debe ingresarse la dirección del remitente")
	private String address;
	
	private long tel;
	
	public sender(@NotNull(message = "Debe ingresarse nombre del establecimiento") String storeName,
			@NotNull(message = "Debe ingresarse nombre del remitente") String nameSender, 
			@NotNull(message = "Debe ingresarse la direccion del remitente") String address,
			long tel) {
		super();
		this.storeName = storeName;
		this.nameSender = nameSender;
		this.address = address;
		this.tel = tel;
	}
	
	public sender() {}
	
	public String getStoreName() {
		return this.storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getNameSender() {
		return this.nameSender;
	}

	public void setNameSender(String nameSender) {
		this.nameSender = nameSender;
	}
	
	public String getAddress() {
		return this.address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}

	public long getTel() {
		return this.tel;
	}

	public void setTel(long tel) {
		this.tel = tel;
	}
	
}
