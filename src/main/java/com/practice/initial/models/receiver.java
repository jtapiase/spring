package com.practice.initial.models;

import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="receivers")
public class receiver {
	
	@NotNull(message = "Debe ingresarse nombre del establecimiento")
	private String storeName;
	
	@NotNull(message = "El destinatario no puede estar vacío")
	private String receiverName;
	
	@NotNull(message = "Se debe ingresar una dirección")
	private String address;
	
	@NotNull(message = "Se debe ingresar ciudad")
	private String city;
	
	private long tel;
	
	public receiver(String storeName, @NotNull(message = "El destinatario no puede estar vacío") String receiverName,
			@NotNull(message = "Se debe ingresar una dirección") String address,
			@NotNull(message = "Se debe ingresar ciudad") String city, long tel) {
		super();
		this.storeName = storeName;
		this.receiverName = receiverName;
		this.address = address;
		this.city = city;
		this.tel = tel;
	}

	public receiver() {}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public long getTel() {
		return tel;
	}

	public void setTel(long tel) {
		this.tel = tel;
	}

	
}
