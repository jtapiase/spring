package com.practice.initial.models;

import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "drivers")
public class driver {
	
	@Id
	private Integer id;
	
	@NotNull(message = "el conductor no debe estar vacío")
	private String name;

	public driver(Integer id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public driver() {}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}	

}
