package com.practice.initial.models;

import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "cars")
public class car {
	
	@Id
	private String carPlate;
	
	@NotNull(message = "Debe ingresarse un nombre")
	private String name;

	public car(String carPlate, String name) {
		this.carPlate = carPlate;
		this.name = name;
	}
	
	public car() {}

	public String getCarPlate() {
		return carPlate;
	}

	public void setCarPlate(String carPlate) {
		this.carPlate = carPlate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
