package com.practice.initial.models;

import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="products")
public class product{
	
	@Id
	private Integer code = 0;
    
	@NotNull(message = "Debes ingresar una cantidad")
    private int cant;
    
	@NotNull(message = "Ingrese una descripción")
    private String description;
    /*
	@NotNull(message = "Remitente invalido")
    private String mySender;
    
	@NotNull(message = "Destinatario invalido")
    private String myReceiver;
    */
	@NotNull(message="ingresa valor")
    private int value;

    public product() {}
    /*
    public product(int cant, String desc, int value, String sender, String receiver){
        aumentarCode();
        this.cant = cant;
        this.description = desc;
        this.value = value;
        this.mySender = sender;
        this.myReceiver = receiver;
    }
	*/
    public product(int cant, String desc, int value) {
        this.cant = cant;
        this.description = desc;
        this.value = value;
    }
    
	public Integer getCode() {
		return this.code;
	}
	public void setCode(Integer code) {
		this.code = code + 1;
	}
	public int getCant() {
		return this.cant;
	}
	public void setCant(int cant) {
		this.cant = cant;
	}
	public String getDescription() {
		return this.description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getValue() {
		return this.value;
	}
	public void setValue(int value) {
		this.value = value;
	}
    
    
    /*
    public String getMyReceiver() {
		return myReceiver;
	}
    
    public String getMySender() {
		return mySender;
	}
    
    public void setMySender(String mySender) {
		this.mySender = mySender;
	}
    
    public void setMyReceiver(String myReceiver) {
		this.myReceiver = myReceiver;
	}
	*/
}