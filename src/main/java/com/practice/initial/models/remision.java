package com.practice.initial.models;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;

@Document(collection = "remisions")
public class remision {
	
	@Id
	private Integer code = 0000; 
	
	@NotNull(message="debe ingresar un recibo")
	Integer billCode;
	
	@NotNull(message="Debe ingresar un conductor")
	driver driver;
	
	@NotNull(message = "Debe ingresarse un vehiculo")
	car car;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
	LocalDateTime date;
	
	public remision(Integer billCode, driver driver, car car) {
		this.billCode = billCode;
		this.driver = driver;
		this.car = car;
		this.date = LocalDateTime.now();
	}
	
	public remision() {}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code + 1;
	}

	public Integer getBillCode() {
		return this.billCode;
	}

	public void setBillCode(Integer billCode) {
		this.billCode = billCode;
	}

	public driver getDriver() {
		return driver;
	}

	public void setDriver(driver driver) {
		this.driver = driver;
	}

	public car getCar() {
		return car;
	}

	public void setCar(car car) {
		this.car = car;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate() {
		this.date = LocalDateTime.now();
	}
	

	
}
