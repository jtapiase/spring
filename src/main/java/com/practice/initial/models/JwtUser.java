package com.practice.initial.models;

//import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class JwtUser {
    private String userName;
    private String role;
    private String password;

    public JwtUser() {}
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
/*
    public void setId(Integer id) {
        this._id = id;
    }
*/
    public void setRole(String role) {
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }
/*
    public Integer getId() {
        return _id;
    }
*/
    public String getRole() {
        return role;
    }
    
    public void setPassword(String pass) {
    	this.password = pass;
    }
    
    public String getPassword() {
    	return this.password;
    }
    
    @Override
    public String toString() {
    	return "UserName: " + this.userName +
    			"\nRole: " + this.role + 
    			"\nPassword: " + this.password;
    }
}
