package com.practice.initial.models;

import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.practice.initial.tools.LocalDateTimeDeserializer;
import com.practice.initial.tools.LocalDateTimeSerializer;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Document(collection="bills")
public class bill implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    public Integer code = 888888;
    
    @NotNull(message = "Los productos no pueden estar vacíos")
    private List<product> products = new ArrayList<product>();
    
    @NotNull(message = "el destinatario no puede estar vacío")
    private receiver receiver;
    
    @NotNull(message = "el remitente no puede estar vacío")
    private sender sender;
    
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private transient LocalDateTime date;
    
    public bill(){}

    public bill(List<product> productos, receiver receiver, sender sender) {
    	this.products = productos;
    	this.receiver = receiver;
    	this.sender = sender;
    	this.date = LocalDateTime.now();
    }
    
    public Integer getCode() {
		return this.code;
	}

	public void setCode(Integer code) {
		this.code = code+1;
	}

	public List<product> getProducts() {
		return this.products;
	}

	public void setProducts(List<product> products) {
		this.products = products;
	}

	public receiver getReceiver() {
		return this.receiver;
	}

	public void setReceiver(receiver receiver) {
		this.receiver = receiver;
	}

	public sender getSender() {
		return this.sender;
	}

	public void setSender(sender sender) {
		this.sender = sender;
	}

	public LocalDateTime getDate() {
		return this.date;
	}

	public void setDate() {
		this.date = LocalDateTime.now();
	}

	@Override
    public String toString() {
    	return "Bill:\n\tStoreSender: "+this.getSender().getStoreName()+
    			"\n\tNameSender: "+ this.getSender().getNameSender()+
    			"\n\tStoreReceiver: " + this.getReceiver().getStoreName()+
    			"\n\tReceiverName: " + this.getReceiver().getReceiverName()+
    			"\n\tID: " + this.getCode();
    }
}