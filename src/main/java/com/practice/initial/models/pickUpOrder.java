package com.practice.initial.models;

import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.practice.initial.tools.LocalDateTimeDeserializer;
import com.practice.initial.tools.LocalDateTimeSerializer;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Document(collection="orders")
public class pickUpOrder implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    public Integer code = 888888;
    
    @NotNull(message = "Los productos no pueden estar vacíos")
    private List<product> products = new ArrayList<product>();
    
    @NotNull(message = "el destinatario no puede estar vacío")
    private receiver receiver;
    
    @NotNull(message = "el remitente no puede estar vacío")
    private sender sender;
    
    @NotNull(message = "el conductor no puede estar vacío")
    private driver driver;
    
    @NotNull(message = "el vehiculo no puede estar vacío")
    private car car;
    
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private transient LocalDateTime date;
    
    public pickUpOrder(){}

    public pickUpOrder(List<product> productos, receiver receiver, sender sender, driver driver, car car) {
    	this.products = productos;
    	this.receiver = receiver;
    	this.sender = sender;
    	this.driver = driver;
    	this.car = car;
    	this.date = LocalDateTime.now();
    }
    
    public Integer getCode() {
		return this.code;
	}

	public void setCode(Integer code) {
		this.code = code+1;
	}

	public List<product> getProducts() {
		return this.products;
	}

	public void setProducts(List<product> products) {
		this.products = products;
	}

	public receiver getReceiver() {
		return this.receiver;
	}

	public void setReceiver(receiver receiver) {
		this.receiver = receiver;
	}

	public sender getSender() {
		return this.sender;
	}

	public void setSender(sender sender) {
		this.sender = sender;
	}

	public LocalDateTime getDate() {
		return this.date;
	}

	public void setDate() {
		this.date = LocalDateTime.now();
	}

	public driver getDriver() {
		return driver;
	}

	public void setDriver(driver driver) {
		this.driver = driver;
	}

	public car getCar() {
		return car;
	}

	public void setCar(car car) {
		this.car = car;
	}

	@Override
    public String toString() {
    	return "Bill:\n\tstoreSender: "+this.getSender().getStoreName()+
    			"\n\tnameSender: "+ this.getSender().getNameSender()+
    			"\n\tstoreReceiver" + this.getReceiver().getStoreName()+
    			"\n\treceiverName" + this.getReceiver().getReceiverName();
    }
}