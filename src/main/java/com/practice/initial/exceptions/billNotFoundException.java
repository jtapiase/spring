package com.practice.initial.exceptions;

public class billNotFoundException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public billNotFoundException(String message) {
		super(message);
	}
}
