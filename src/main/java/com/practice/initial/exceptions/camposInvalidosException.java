package com.practice.initial.exceptions;

public class camposInvalidosException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public camposInvalidosException(String message) {
		super(message);
	}

}
