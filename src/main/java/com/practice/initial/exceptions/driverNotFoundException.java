package com.practice.initial.exceptions;

public class driverNotFoundException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public driverNotFoundException(String message) {
		super(message);
	}
	
}
