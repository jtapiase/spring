package com.practice.initial.exceptions;

public class receiverNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public receiverNotFoundException(String message) {
		super(message);
	}
	
}
