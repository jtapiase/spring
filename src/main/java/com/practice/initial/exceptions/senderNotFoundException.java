package com.practice.initial.exceptions;

public class senderNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public senderNotFoundException(String message) {
		super(message);
	}
	
}
